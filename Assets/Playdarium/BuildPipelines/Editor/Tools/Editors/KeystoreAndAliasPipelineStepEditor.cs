using Playdarium.BuildPipelines.Tools.Impls;
using UnityEditor;

namespace Playdarium.BuildPipelines.Tools.Editors
{
	[CustomEditor(typeof(KeystoreAndAliasPipelineStep))]
	public class KeystoreAndAliasPipelineStepEditor : Editor
	{
		private SerializedProperty _useSecureSp;
		private SerializedProperty _useCustomKeystoreSp;


		private void OnEnable()
		{
			_useSecureSp = serializedObject.FindProperty("useSecure");
			_useCustomKeystoreSp = serializedObject.FindProperty("useCustomKeystore");
			// var keystoreNameSp = serializedObject.FindProperty("keystoreName");
			// var keystorePasswordSp = serializedObject.FindProperty("keystorePassword");
			// var aliasNameSp = serializedObject.FindProperty("aliasName");
			// var aliasPasswordSp = serializedObject.FindProperty("aliasPassword");
		}

		public override void OnInspectorGUI()
		{
			if (!_useSecureSp.boolValue)
			{
				base.OnInspectorGUI();
				return;
			}

			EditorGUILayout.PropertyField(_useSecureSp);
			EditorGUILayout.PropertyField(_useCustomKeystoreSp);
			EditorGUILayout.HelpBox("Secure values is used", MessageType.Info);
			using (var change = new EditorGUI.ChangeCheckScope())
			{
				var keystoreName = EditorGUILayout.TextField(
					"Keystore Name",
					KeystoreAndAliasPipelineStep.Settings.KeystoreName
				);
				var keystorePassword = EditorGUILayout.PasswordField(
					"Keystore Password",
					KeystoreAndAliasPipelineStep.Settings.KeystorePassword
				);
				var aliasName = EditorGUILayout.TextField(
					"Alias Name",
					KeystoreAndAliasPipelineStep.Settings.AliasName
				);
				var aliasPassword = EditorGUILayout.PasswordField(
					"Alias Password",
					KeystoreAndAliasPipelineStep.Settings.AliasPassword
				);

				if (change.changed)
				{
					KeystoreAndAliasPipelineStep.Settings.KeystoreName = keystoreName;
					KeystoreAndAliasPipelineStep.Settings.KeystorePassword = keystorePassword;
					KeystoreAndAliasPipelineStep.Settings.AliasName = aliasName;
					KeystoreAndAliasPipelineStep.Settings.AliasPassword = aliasPassword;
				}
			}
		}
	}
}