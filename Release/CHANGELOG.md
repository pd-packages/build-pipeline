# Changelog

---

## [v1.0.11](https://gitlab.com/pd-packages/build-pipeline/-/tags/v1.0.11)

### Added

- Add use secure values flag to KeystoreAndAliasPipelineStep

---

## [v1.0.10](https://gitlab.com/pd-packages/build-pipeline/-/tags/v1.0.10)

### Fixed

- Add define UNITY_IOS to iOSPostBuildProcessor

---

## [v1.0.9](https://gitlab.com/pd-packages/build-pipeline/-/tags/v1.0.9)

### Added

- Possibility to launch a pipeline ScriptableObject

---

## [v1.0.8](https://gitlab.com/pd-packages/build-pipeline/-/tags/v1.0.8)

### Fixed

- iOS post-build processor declares ITSAppUsesNonExemptEncryption to NO

---

## [v1.0.7](https://gitlab.com/pd-packages/build-pipeline/-/tags/v1.0.7)

### Fixed

- iOS post-build processor set marketing version for target UnityFramework.framework

---

## [v1.0.6](https://gitlab.com/pd-packages/build-pipeline/-/tags/v1.0.6)

### Added

- iOS post-build processor where set a marketing version and ITSAppUsesNonExemptEncryption to NO

---

## [v1.0.5](https://gitlab.com/pd-packages/build-pipeline/-/tags/v1.0.5)

### Added

- iOS build auto signing flag

---

## [v1.0.4](https://gitlab.com/pd-packages/build-pipeline/-/tags/v1.0.4)

### Changes

- Change iOS export options certificate signing style to automatically

---

## [v1.0.3](https://gitlab.com/pd-packages/build-pipeline/-/tags/v1.0.3)

### Changes

- Remove provision paths from iOSBuild step

---

## [v1.0.2](https://gitlab.com/pd-packages/build-pipeline/-/tags/v1.0.2)

### Changes

- Move a project to GitLab

---

## [v1.0.1](https://github.com/releases/tag/v1.0.1)

### Fixed

- Trigger build

---

## [v1.0.0](https://github.com/releases/tag/v1.0.0)

### Added

- Project init

---

## [v0.0.0](https://github.com/releases/tag/v0.0.0)

### Added

- For new features.

### Changed

- For changes in existing functionality.

### Deprecated

- For soon-to-be removed features.

### Removed

- For now removed features.

### Fixed

- For any bug fixes.

### Security

- In case of vulnerabilities.
