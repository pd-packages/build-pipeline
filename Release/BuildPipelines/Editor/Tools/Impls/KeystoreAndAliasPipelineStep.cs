using System;
using System.Text;
using Playdarium.BuildPipelines.Parameters;
using UnityEditor;
using UnityEngine;

namespace Playdarium.BuildPipelines.Tools.Impls
{
	[CreateAssetMenu(menuName = "BuildPipeline/Steps/KeystoreAndAlias", fileName = "KeystoreAndAlias")]
	public class KeystoreAndAliasPipelineStep : APipelineStep
	{
		[SerializeField] private bool useSecure;
		[SerializeField] private bool useCustomKeystore;
		[SerializeField] private string keystoreName;
		[SerializeField] private string keystorePassword;
		[SerializeField] private string aliasName;
		[SerializeField] private string aliasPassword;

		public override void Execute(BuildParameterHolder parameterHolder, Action onComplete)
		{
			if (!useSecure)
				SetKeystoreData(useCustomKeystore, keystoreName, keystorePassword, aliasName, aliasPassword);
			else
				SetKeystoreData(
					useCustomKeystore,
					Settings.KeystoreName,
					Settings.KeystorePassword,
					Settings.AliasName,
					Settings.AliasPassword
				);

			onComplete();
		}

		private static void SetKeystoreData(
			bool useCustomKeystore,
			string keystoreName,
			string keystorePassword,
			string aliasName,
			string aliasPassword
		)
		{
			PlayerSettings.Android.useCustomKeystore = useCustomKeystore;
			PlayerSettings.Android.keystoreName = string.IsNullOrEmpty(keystoreName) ? null : keystoreName;
			PlayerSettings.Android.keystorePass = string.IsNullOrEmpty(keystorePassword) ? null : keystorePassword;
			PlayerSettings.Android.keyaliasName = string.IsNullOrEmpty(aliasName) ? null : aliasName;
			PlayerSettings.Android.keyaliasPass = string.IsNullOrEmpty(aliasPassword) ? null : aliasPassword;
		}

		public static class Settings
		{
			private static readonly string KeystoreNameKey =
				$"KeystoreAndAlias.{PlayerSettings.productGUID}.KeystoreName";

			private static readonly string
				KeystorePwdKey = $"KeystoreAndAlias.{PlayerSettings.productGUID}.KeystorePwd";

			private static readonly string AliasNameKey = $"KeystoreAndAlias.{PlayerSettings.productGUID}.AliasName";
			private static readonly string AliasPwdKey = $"KeystoreAndAlias.{PlayerSettings.productGUID}.AliasPwd";

			public static string KeystoreName
			{
				get => GetSecureValue(KeystoreNameKey);
				set => SetSecureValue(KeystoreNameKey, value);
			}

			public static string KeystorePassword
			{
				get => GetSecureValue(KeystorePwdKey);
				set => SetSecureValue(KeystorePwdKey, value);
			}

			public static string AliasName
			{
				get => GetSecureValue(AliasNameKey);
				set => SetSecureValue(AliasNameKey, value);
			}

			public static string AliasPassword
			{
				get => GetSecureValue(AliasPwdKey);
				set => SetSecureValue(AliasPwdKey, value);
			}

			private static void SetSecureValue(string key, string value)
			{
				var str = Convert.ToBase64String(Encoding.UTF8.GetBytes(value));
				EditorPrefs.SetString(key, str);
			}

			private static string GetSecureValue(string key)
			{
				var value = EditorPrefs.GetString(key, string.Empty);
				return !string.IsNullOrEmpty(value) ? Encoding.UTF8.GetString(Convert.FromBase64String(value)) : value;
			}
		}
	}
}