#if UNITY_EDITOR && UNITY_IOS
using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using UnityEngine;

namespace Playdarium.BuildPipelines.PostProcess.iOS
{
	public class iOSPostBuildProcessor
	{
		[PostProcessBuild]
		public static void OnPostProcessBuild(BuildTarget target, string pathToBuiltProject)
		{
			if (target != BuildTarget.iOS)
				return;

			DeclareNoneAppEncryption(pathToBuiltProject);

			var projectPath = pathToBuiltProject + "/Unity-iPhone.xcodeproj/project.pbxproj";
			var pbxProject = new PBXProject();
			pbxProject.ReadFromFile(projectPath);

			SetMarketingVersion(pbxProject, pbxProject.GetUnityMainTargetGuid());
			SetMarketingVersion(pbxProject, pbxProject.GetUnityFrameworkTargetGuid());
			Debug.Log("Set MARKETING_VERSION to: " + Application.version);

			pbxProject.WriteToFile(projectPath);
			Debug.Log("Xcode project updated successfully.");
		}

		private static void SetMarketingVersion(PBXProject project, string targetGuid)
		{
			project.SetBuildProperty(targetGuid, "MARKETING_VERSION", Application.version);
		}

		private static void DeclareNoneAppEncryption(string pathToBuiltProject)
		{
			var plistPath = Path.Combine(pathToBuiltProject, "Info.plist");
			var plist = new PlistDocument();
			plist.ReadFromFile(plistPath);
			var rootDict = plist.root;
			rootDict.SetBoolean("ITSAppUsesNonExemptEncryption", false);
			Debug.Log("Set ITSAppUsesNonExemptEncryption to: NO");
			File.WriteAllText(plistPath, plist.WriteToString());
		}
	}
}
#endif